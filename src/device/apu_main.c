/*
 * Copyright (C) 2020, GSI Technology, Inc. All rights reserved.
 *
 * This software source code is the sole property of GSI Technology, Inc.
 * and is proprietary and confidential.
 */


//#include <gsi/libgvml_base.h>
#include <gsi/libsys.h>
#include <gsi/gal-fast-funcs.h>
#include <gsi/libgal.h>
#include <gsi/libgvml.h>
#include <gsi/libgvml_element_wise.h>
#include "libgipl_dev_profiling.h"
#include "ndg_common_defs.h"
#include "libndg_dev_dma.h"

void gvml_init_once(void);			// Not found in simulator mode

GAL_INCLUDE_INIT_TASK



GAL_TASK_ENTRY_POINT(apu_task, in, out)
{
	gsi_info("starting apu_task");
	gvml_init_once();
	gal_fast_l2dma_async_memcpy_init(0);
	gal_fast_l2dma_async_memcpy_init(1); 
	int status = 0;
	uint core_id = gal_fast_apuc_id();
	uint shift_size, core_offset, K;
	core_offset = _32K *4* core_id; // number of bitlines * number of groups (chunks) per bit line
	u16 *in_arr, *out_arr ;
	struct ndg_ctx_in *ctx = (struct ndg_ctx_in*)in; 
	in_arr =  (u16*)gal_mem_handle_to_apu_ptr(ctx->in_array)  ;
	out_arr = (u16*)gal_mem_handle_to_apu_ptr(ctx->out_array) ;
	chunk *g_chunks = ((chunk*) gal_mem_handle_to_apu_ptr(ctx->in_array)) + core_offset;
	u16 *g_chunks_u16 = (u16*) g_chunks;
	for(size_t c = 0 ; c< 32768* CHUNK_LENGTH/2 ;c++){
			g_chunks_u16[c] = 0xabcd + c;
	}
	ndg_chunk_mem_to_l1(0, g_chunks);
	
	return status;
}

