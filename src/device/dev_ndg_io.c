/*
 * Copyright (C) 2020, GSI Technology, Inc. All rights reserved.
 *
 * This software source code is the sole property of GSI Technology, Inc.
 * and is proprietary and confidential.
 */


#include <gsi/libsys.h>
#include <gsi/gal-fast-funcs.h>
#include <gsi/libgal.h>
#include <gsi/libgvml.h>
#include <gsi/libgvml_memory.h>
#include <gsi/libgvml_debug.h>
#include <gsi/libgvml_element_wise.h>
#include "ndg_common_defs.h"
#include "libndg_dev_dma.h"
#include "libndg_dev_defs.h"
#include "ndg_io.h"
#include "dev_ndg_io.h"

void ndg_chunk_mem_to_l1(enum l1_set l1_set_start, chunk *src){
    for(u8 bank_id = 0 ; bank_id < GSI_L1_NUM_BANKS; bank_id++){
        for(enum l1_grp grp = l1_grp0; grp <= l1_grp3; grp++){

            ndg_dma_l4_to_l2_16k(0 , (u16*) (src + grp * _4K + bank_id * _16K ) );           // apc_0
            ndg_dma_l4_to_l2_16k(1 , (u16*) (src + grp * _4K + bank_id * _16K + _32K ) );    // apc_1
            // Load CHUNK_LENGTH(8) bytes to CHUNK_LENGTH/2 VMRs starting from vmr_start
            copy_chunk_l2_to_l1( l1_set_start, bank_id, grp);
        }
    }
        //  DEBUGGING code
    /*   
        enum gvml_vr16 vr = GVML_VR16_3;
        chunk dst= {
            .data = {0},
        };
        u16 checked_entry = 0;
        int i = 0;
        gvml_reset_16(vr);
        gvml_reset_16(vr+1);
        for(i=0 ; i < 8 && gal_fast_apuc_id() == 0; i ++){
            copy_chunk_l1_to_mmb(vr, l1_set_start+i);
            u16 read0 = gvml_get_entry_16(vr, checked_entry);
            u16 read1 = gvml_get_entry_16(vr + 1, checked_entry);
            dst.data[i] = (read1 & 0x000f) << 4 | (read0 & 0x000f);
            gsi_log("entry %d chunk.data[%d] = %x", checked_entry, i, dst.data[i]);
        }
        // for(i=0 ; i < 4; i ++)        
        //     gvml_load_16(vr+i, vmr_start+i);
        // u16 entry =0;
        // i=0;
        // while( i < 4 && gal_fast_apuc_id() == 0)
        // {   
        //     u8 checked_bank = i % GSI_L1_NUM_BANKS;
        //     u16* src_u16 = (u16*)(src + _4K * checked_bank);
        //     for(int j = 0 ;j < CHUNK_LENGTH/2 ;j++){
        //         entry = gvml_get_entry_16(vr+j, i);
        //         if(entry)
        //             gsi_log("got %04x in entry %d vmr %d. Expected %04x", entry, i, j, src_u16[ j]); // Works for coulmns 0-3. not further
        //     }
        //     i++;
        // }
    */
}

