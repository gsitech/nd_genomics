/*
 * Copyright (C) 2020, GSI Technology, Inc. All rights reserved.
 *
 * This software source code is the sole property of GSI Technology, Inc.
 * and is proprietary and confidential.
 */
#ifndef ___NDG_DEV_IO_APL_H__
#define ___NDG_DEV_IO_APL_H__
enum l1_set;
enum l1_grp;

void copy_chunk_l2_to_l1( enum l1_set l1_set, u8 l1_bank_id, enum l1_grp group);
void copy_chunk_l1_to_mmb(enum gvml_vr16 vrdst, enum l1_set l1_set);

#endif /* ___NDG_DEV_IO_APL_H__ */
