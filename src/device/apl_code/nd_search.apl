APL_C_INCLUDE <gsi/preproc_defs.h>
APL_C_INCLUDE <gsi/libapl.h>
APL_C_INCLUDE <gsi/libsys.h>
APL_C_INCLUDE <gsi/gal-fast-funcs.h>

APL_C_INCLUDE <ndg_common_defs.h>

#include "gvml_apl_defs.apl.h"

APL_FRAG invert_frag(RN_REG vr)
{
	SM_REG_0: RL =  SB[vr];
	SM_REG_0: SB[vr] =  INV_RL;
};


void _gvml_invert(int vr)
{
	apl_set_rn_reg(RN_REG_0, vr);
	RUN_FRAG_ASYNC(invert_frag(vr = RN_REG_0));
}