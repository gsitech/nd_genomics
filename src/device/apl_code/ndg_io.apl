
APL_C_INCLUDE <gsi/preproc_defs.h>
APL_C_INCLUDE <gsi/libapl.h>
APL_C_INCLUDE <gsi/libsys.h>
APL_C_INCLUDE <gsi/gal-fast-funcs.h>
APL_C_INCLUDE <gsi/libgvml.h>

APL_C_INCLUDE <ndg_common_defs.h>
APL_C_INCLUDE <libndg_dev_defs.h>
APL_C_INCLUDE <ndg_io.h>

#include "gvml_apl_defs.apl.h"

APL_FRAG l2_end(VOID)
{
	L2_END;
};

APL_FRAG copy_l2_to_l1_byte(L1_REG dst, L1_REG parity_dst, L2_REG src)
{
    	LGL = src + 0;
	{
		dst + 0 = LGL;
		LGL = src + 1;
	}
	{
		dst + 1 = LGL;
		LGL = src + 2;
	}
	{
		dst + 2 = LGL;
		LGL = src + 3;
	}
	{
		dst + 3 = LGL;
		LGL = src + 4;
	}
	{
		dst + 4 = LGL;
		LGL = src + 5;
		//dst + 1,0 = LGL;
	}
	{
		dst + 5 = LGL;
		LGL = src + 6;
		//dst + 1,1 = LGL;
	}
	{
		dst + 6 = LGL;
		LGL = src + 7;
		//dst + 1,2 = LGL;
	}
	{
		dst + 7 = LGL;
		//LGL = src + 8;
		//dst + 1,3 = LGL;
	}
    
		//parity_dst = LGL;
};

static inline __attribute__((always_inline)) u8 encode_l2_addr(uint byte_idx, uint bit_idx)
{

    return (u8)((byte_idx << GSI_L2_CTL_ROW_ADDR_BIT_IDX_BITS) | bit_idx);
}


void copy_chunk_l2_to_l1( enum l1_set l1_set, u8 l1_bank_id, enum l1_grp group)
{
	int i;
	uint l1_parity_grp, l1_parity_row;
	u8 l2_addr;
    gal_fast_l2dma_l2_ready_rst_all();
	for (i = 0; i < CHUNK_LENGTH; i++, l1_set++) {
		l2_addr = encode_l2_addr(i, 0);
		apl_set_l1_reg_ext(L1_ADDR_REG0, l1_bank_id, (uint)group, l1_set << 4);
     	apl_set_l2_reg(L2_ADDR_REG0, l2_addr);
		RUN_FRAG_ASYNC(copy_l2_to_l1_byte(dst = L1_ADDR_REG0, parity_dst = L1_ADDR_REG1, src = L2_ADDR_REG0));
	}
    // set l2_ready
    RUN_FRAG_ASYNC(l2_end());
}

// ----------------------------------------------------------------------
// ----------------------------- DEBUG CODE -----------------------------
// ----------------------------------------------------------------------
// copy chunk from L1 to MMb for debugging purposes:
APL_FRAG copy_l1_to_mmb_byte(RN_REG dst_lsb, RN_REG dst_msb, L1_REG src)
{
		GGL = src + 0;
	{
		SM_0X1111: SB[dst_lsb] = GGL;
		GGL = src + 1;
	}
	{
		SM_0X1111 << 1: SB[dst_lsb] = GGL;
		GGL = src + 2;
	}
	{
		SM_0X1111 << 2: SB[dst_lsb] = GGL;
		GGL = src + 3;
	}
	{
		SM_0X1111 << 3: SB[dst_lsb] = GGL;
		GGL = src + 4;
	}
	{
		SM_0X1111: SB[dst_msb] = GGL;
		GGL = src + 5;
	}
	{
		SM_0X1111 << 1: SB[dst_msb] = GGL;
		GGL = src + 6;
	}
	{
		SM_0X1111 << 2: SB[dst_msb] = GGL;
		GGL = src + 7;
	}
	{
		SM_0X1111 << 3: SB[dst_msb] = GGL;
	}
};

void copy_chunk_l1_to_mmb(enum gvml_vr16 vrdst, enum l1_set l1_set)
{
    apl_set_l1_reg(L1_ADDR_REG0, l1_set << 4);
    apl_set_rn_reg(RN_REG_0, vrdst);
    apl_set_rn_reg(RN_REG_1, vrdst+1);
    RUN_FRAG_ASYNC(copy_l1_to_mmb_byte(dst_lsb = RN_REG_0, dst_msb = RN_REG_1, src = L1_ADDR_REG0));
}


