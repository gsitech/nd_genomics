/*
 * Copyright (C) 2020, GSI Technology, Inc. All rights reserved.
 *
 * This software source code is the sole property of GSI Technology, Inc.
 * and is proprietary and confidential.
 */
#ifndef ___NDG_DEV_IO_H__
#define ___NDG_DEV_IO_H__

void ndg_chunk_mem_to_l1(enum l1_set l1_set_start, chunk *src);
#endif /* ___NDG_DEV_DEFS_H__ */
