/*
 * Copyright (C) 2020, GSI Technology, Inc. All rights reserved.
 *
 * This software source code is the sole property of GSI Technology, Inc.
 * and is proprietary and confidential.
 */
#ifndef ___NDG_DEV_DEFS_H__
#define ___NDG_DEV_DEFS_H__

enum l1_set{
    NDG_L1_SET0 = 0,
    NDG_L1_SET1 = 1,
    NDG_L1_SET2 = 2,
    NDG_L1_SET3 = 3,
    NDG_L1_SET4 = 4,
    NDG_L1_SET5 = 5,
    NDG_L1_SET6 = 6,
    NDG_L1_SET7 = 7,
    NDG_L1_SET8 = 8,
    NDG_L1_SET9 = 9,
    NDG_L1_SET10 = 10,
    NDG_L1_SET11 = 11,
    NDG_L1_SET12 = 12,
    NDG_L1_SET13 = 13,
    NDG_L1_SET14 = 14,
    NDG_L1_SET15 = 15,
    NDG_L1_SET16 = 16,
    NDG_L1_SET17 = 17,
    NDG_L1_SET18 = 18,
    NDG_L1_SET19 = 19,
    NDG_L1_SET20 = 20,
    NDG_L1_SET21 = 21,
    NDG_L1_SET22 = 22,
    NDG_L1_SET23 = 23,
};

enum l1_grp{
    l1_grp0 = 0,
    l1_grp1 = 1,
    l1_grp2 = 2,
    l1_grp3 = 3,
};
#endif /* ___NDG_DEV_DEFS_H__ */
