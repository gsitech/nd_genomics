#ifndef ___NDG_COMMON_DEFS_H__
#define ___NDG_COMMON_DEFS_H__

#define SEED_LENGTH 		8				// Length of seed in bytes
#define CHUNK_LENGTH 		SEED_LENGTH		// Length of chunk in bytes
#define READ_LENGTH			32				// Length of Read in bytes

typedef struct genome_chunk{
	uint8_t					data[CHUNK_LENGTH];
} chunk;

struct ndg_ctx_in{	
	uint64_t				dev_buf_ctx;					// handle to this context 
	uint64_t				dev_buf_out;					// handle to this context 
	
	uint64_t				in_array;				
	uint64_t				out_array;				

	uint32_t				num_operations;
	uint16_t				val1;
	uint16_t				val2;
	unsigned long int 		ctx_id;							// gdl_ctx_id

} __attribute__((packed));

struct ndg_ctx_out{	
	uint64_t				cycles_out[4];
	uint64_t				instructions_out[4];
} __attribute__((packed));

#endif /* ___NDG_COMMON_DEFS_H__ */