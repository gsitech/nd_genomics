#ifndef ___LIBNDG_H__
#define ___LIBNDG_H__

struct ndg_ctx_in;
// #define HOST_VERBOSE        // comment for performance.

int ndg_create_ctx(void **ndg_cntx, gdl_context_handle_t gdl_ctx_id );
int ndg_destory_ctx(void *ndg_cntx );
int ndg_run_test(struct ndg_ctx_in  *ctx);

#endif /* ___LIBNDG_H__ */