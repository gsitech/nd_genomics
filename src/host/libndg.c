/*
 * Copyright (C) 2020, GSI Technology, Inc. All rights reserved.
 *
 * This software source code is the sole property of GSI Technology, Inc.
 * and is proprietary and confidential.
 */
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <gsi/libgdl.h>
#include <gsi/libsys.h>
#include <gsi/gsi_sim_config.h>

#include <sys/time.h>   	// for time()
#include "libndg.h"
#include "host_functions.h"
#include "ndg_common_defs.h"
#include "nd_genom-defs.h" // file is created during build process

int ndg_create_ctx(void **ndg_cntx, gdl_context_handle_t gdl_ctx_id )
{
	int ret =0;	
	gdl_mem_handle_t dev_buf_ctx =  gdl_mem_alloc_nonull(gdl_ctx_id, sizeof(struct ndg_ctx_in), GDL_CONST_MAPPED_POOL);
	*ndg_cntx = (struct ndg_ctx_in*)gdl_mem_handle_to_host_ptr(dev_buf_ctx);
	struct ndg_ctx_in *new_ctx = *ndg_cntx ;
	new_ctx->ctx_id = gdl_ctx_id;
	new_ctx->dev_buf_ctx = dev_buf_ctx;
	new_ctx->dev_buf_out = gdl_mem_alloc_nonull(gdl_ctx_id, sizeof(struct ndg_ctx_out), GDL_CONST_MAPPED_POOL);
	// allocate array of input
	new_ctx->in_array = gdl_mem_alloc_nonull(gdl_ctx_id, GSI_MAX_APUC_COUNT /* APUCs on chi[*/
															* 32768			/* Bit lines per APUC*/ 
															* 4				/* Groups per bit line*/
															*sizeof(chunk), /* One chunk per group */
															 GDL_CONST_MAPPED_POOL);

	return ret;
}

int ndg_destory_ctx(void *ndg_cntx ){
	int ret = 0;
	struct ndg_ctx_in *ctx = (struct ndg_ctx_in *) ndg_cntx;

	if(gdl_mem_handle_to_host_ptr(ctx->in_array)) gdl_mem_free(ctx->in_array);
	gdl_mem_free(ctx->dev_buf_out );
	gdl_mem_free(ctx->dev_buf_ctx);

	return ret;
}

int ndg_run_test(struct ndg_ctx_in  *ctx){
	int ret = 0;
	struct gsi_task_desc my_task[GSI_MAX_APUC_COUNT];
	struct gdl_mem_buffer dynamic_mem_table;
	for(int i=0 ; i < GSI_MAX_APUC_COUNT; i++)
	{	
		my_task[i] = *gdl_task_desc_create(ctx->ctx_id, GDL_TASK(apu_task), ctx->dev_buf_ctx, ctx->dev_buf_out, i);		
	}
	dynamic_mem_table.ptr = ctx->dev_buf_ctx;
	dynamic_mem_table.size = sizeof(struct ndg_ctx_in);
	ret = gdl_schedule_batch_timeout(my_task, GSI_MAX_APUC_COUNT, &dynamic_mem_table, 1, NULL , 0, GDL_USER_MAPPING);
	if(ret)
	{
		printf("error during task with error %d\n", ret);
	}

	return ret;
}