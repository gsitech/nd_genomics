/*
 * Copyright (C) 2020, GSI Technology, Inc. All easts reserved.
 *
 * This software source code is the sole property of GSI Technology, Inc.
 * and is proprietary and confidential.
 */

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <gsi/libgdl.h>
#include <gsi/libsys.h>
#include <gsi/gsi_sim_config.h>
#include <sys/time.h>   	// for time()

#include "libndg.h"
#include "ndg_common_defs.h"

#define NUM_CTXS 1

uint ctx_id = 1;    


static struct gsi_sim_contexts g_ctxs[NUM_CTXS] = {
	{
		.apu_count = 1,
		.apucs_per_apu = 4,
		.mem_size = 0x40000000,
	}
};


double main(int argc, char *argv[])
{
	int ret = 0;
	unsigned int num_ctxs;
	long mem_size, num_apucs;
	// unsigned long long *const_mapped_size_recv;
	// unsigned long long *dynamic_mapped_size_recv;

	struct gdl_context_desc contexts_desc[GDL_MAX_NUM_CONTEXTS];

	gdl_init();
	gdl_context_count_get(&num_ctxs);
	gdl_context_desc_get(contexts_desc, num_ctxs);

	gdl_context_property_get(contexts_desc[ctx_id].ctx_id, GDL_CONTEXT_MEM_SIZE, &mem_size);
	gdl_context_property_get(contexts_desc[ctx_id].ctx_id, GDL_CONTEXT_NUM_APUCS, &num_apucs);


	ret = gdl_context_alloc(contexts_desc[ctx_id].ctx_id, 0, NULL,NULL);
	if (ret) {
		printf("Failed to allocate GDL context (err = %d)!!!\n", ret);
		return ret;
	}
	void *ctx;
	ret = ndg_create_ctx( &ctx, contexts_desc[ctx_id].ctx_id );
	struct timeval begin_time, end_time;
	gettimeofday(&begin_time, NULL);
	ret = ndg_run_test(ctx);
	gettimeofday(&end_time,NULL);
	double elapsed_time = (double)((end_time.tv_sec - begin_time.tv_sec) + (double)(end_time.tv_usec - begin_time.tv_usec)/(double)1000000 );	
	gdl_context_free(contexts_desc[ctx_id].ctx_id);
	gdl_exit();

	// gsi_sim_destroy_simulator();
	if (ret != 0) {
		printf("\nTest Failed\n");
	} else {
		printf("\nTest Passed\n");
	}
	return ret;
}


