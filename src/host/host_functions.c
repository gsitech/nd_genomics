/*
 * Copyright (C) 2020, GSI Technology, Inc. All rights reserved.
 *
 * This software source code is the sole property of GSI Technology, Inc.
 * and is proprietary and confidential.
 */
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <gsi/libgdl.h>
#include <gsi/libsys.h>
#include "nd_genom-defs.h" // file is created during build process

#include "host_functions.h"
void load_random_to_mem(void* dst, unsigned int size){
	uint32_t *runner = (uint32_t*) dst;
	for(uint32_t i = 0 ; i < size/4; i++){
		runner[i] = (uint32_t) rand();
	}
	uint8_t* char_runner = (uint8_t*)(runner + size/8);
	for(uint32_t i = 0 ; i < size % 8; i++){
		char_runner[i]= (uint8_t) random();
	}
}